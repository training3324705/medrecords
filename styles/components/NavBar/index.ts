import styled from "styled-components";

export const NavBarWrapper = styled.div`
  width: 100%;
  max-width: 1850px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  border: 2px solid grey;
  border-radius: 5px;
  background: white;
  color: black;
  padding: 10px;
  position: fixed;
  z-index: 1;
`;

export const LogoWrapper = styled.div`
  .logo {
    width: 100px;
    height: 100px;
  }
`;

export const LinksWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 16px;
`;
export const Links = styled.div``;
export const LinksText = styled.div`
  font-size: 16px;
`;

export const AccWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 16px;
`;
export const SignInWrapper = styled.div``;
export const SignInButton = styled.button`
  width: 100%;
  max-width: 156px;
  cursor: pointer;
  padding: 0px 10px;
  background: none;
  color: rgb(68, 115, 245);
  border: 1px solid rgba(38, 119, 233, 0.28);
  font-size: 14px;
  font-family: OutfitSemiBold;
  height: 50px;
  border-radius: 16px;
`;
export const RegisterWrapper = styled.div``;
export const RegisterButton = styled.button`
  width: 100%;
  max-width: 156px;
  cursor: pointer;
  padding: 0px 10px;
  background: rgb(68, 115, 245);
  color: rgb(255, 255, 255);
  border: none;
  font-size: 14px;
  font-family: OutfitSemiBold;
  height: 50px;
  border-radius: 16px;
`;
