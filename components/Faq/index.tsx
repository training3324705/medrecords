import React from "react";
import styled from "styled-components";

export const Faq = () => {
  return (
    <FaqMainWrapper>
      <Heading>FAQs</Heading>
      <QuestionsMainWrapper>
        <QuestionsWrapper>
          <Question>Why Use External It Support?</Question>
          <Answer>
            Your records also have the results of medical tests, treatments,
            medicines, and any notes doctors make about you and your health.
            Medical records aren't only about your physical health. They also
          </Answer>
          <LearnMore>Learn More</LearnMore>
        </QuestionsWrapper>
        <QuestionsWrapper>
          <Question>Why Use External It Support?</Question>
          <Answer>
            Your records also have the results of medical tests, treatments,
            medicines, and any notes doctors make about you and your health.
            Medical records aren't only about your physical health. They also
          </Answer>
          <LearnMore>Learn More</LearnMore>
        </QuestionsWrapper>
        <QuestionsWrapper>
          <Question>Why Use External It Support?</Question>
          <Answer>
            Your records also have the results of medical tests, treatments,
            medicines, and any notes doctors make about you and your health.
            Medical records aren't only about your physical health. They also
          </Answer>
          <LearnMore>Learn More</LearnMore>
        </QuestionsWrapper>
        <QuestionsWrapper>
          <Question>Why Use External It Support?</Question>
          <Answer>
            Your records also have the results of medical tests, treatments,
            medicines, and any notes doctors make about you and your health.
            Medical records aren't only about your physical health. They also
          </Answer>
          <LearnMore>Learn More</LearnMore>
        </QuestionsWrapper>
        <QuestionsWrapper>
          <Question>Why Use External It Support?</Question>
          <Answer>
            Your records also have the results of medical tests, treatments,
            medicines, and any notes doctors make about you and your health.
            Medical records aren't only about your physical health. They also
          </Answer>
          <LearnMore>Learn More</LearnMore>
        </QuestionsWrapper>
        <QuestionsWrapper>
          <Question>Why Use External It Support</Question>
          <Answer>
            Your records also have the results of medical tests, treatments,
            medicines, and any notes doctors make about you and your health.
            Medical records aren't only about your physical health. They also
          </Answer>
          <LearnMore>Learn More</LearnMore>
        </QuestionsWrapper>
      </QuestionsMainWrapper>
    </FaqMainWrapper>
  );
};
//styles
const FaqMainWrapper = styled.div`
  padding: 10px;
`;
const Heading = styled.div`
  font-size: 60px;
  text-align: center;
`;

const QuestionsMainWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 10px;
  flex-wrap: wrap;
`;
const QuestionsWrapper = styled.div`
  width: 100%;
  max-width: 700px;
  color: rgb(255, 255, 255);
  padding: 34px 26px 29px 17px;
  background: rgb(8, 25, 58);
  border-radius: 20px;
  cursor: pointer;
`;
const Question = styled.div`
  font-size: 22px;
`;
const Answer = styled.div`
  font-size: 16px;
`;
const LearnMore = styled.div`
  color: blue;
`;
