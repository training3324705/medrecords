import React, { useState } from "react";
import Image from "next/image";
import MedLogo from "../../public/assets/medlogo.jpeg";
import {
  AccWrapper,
  Links,
  LinksText,
  LinksWrapper,
  LogoWrapper,
  NavBarWrapper,
  RegisterButton,
  RegisterWrapper,
  SignInButton,
  SignInWrapper,
} from "../../styles/components/NavBar";

import SignIn from "../SignInModal";
import SignUp from "../SignUpModal";

export const NavBar = () => {
  const [showModal, setShowModal] = useState(false);
  const [showSecondModal, setShowSecondModal] = useState(false);

  const handleSignInClick = () => {
    setShowModal(true);
  };
  const handleSignUpClick = () => {
    setShowSecondModal(true);
  };
  return (
    <NavBarWrapper>
      <LogoWrapper>
        <Image src={MedLogo} alt="Logo" className="logo" />
      </LogoWrapper>
      <LinksWrapper>
        <Links>
          <LinksText>Home</LinksText>
        </Links>
        <Links>
          <LinksText>Features</LinksText>
        </Links>

        <Links>
          <LinksText>FAQs</LinksText>
        </Links>
        <Links>
          <LinksText>Contact Us</LinksText>
        </Links>
      </LinksWrapper>
      <AccWrapper>
        <SignInWrapper>
          <SignInButton onClick={handleSignInClick}>Sign In</SignInButton>
          {showModal && <SignIn show={setShowModal} />}
        </SignInWrapper>
        <RegisterWrapper>
          <RegisterButton onClick={handleSignUpClick}>Register</RegisterButton>
          {showSecondModal && <SignUp show={setShowSecondModal} />}
        </RegisterWrapper>
      </AccWrapper>
    </NavBarWrapper>
  );
};
