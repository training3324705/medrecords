import Image from "next/image";
import React from "react";
import styled from "styled-components";
import HospitalImg from "../../public/assets/hospital.png";

export const Features = () => {
  return (
    <FeaturesMainwrapper>
      <Headingwrapper>Why MedRecords?</Headingwrapper>
      <TextAndHeadingwrapper>
        <Textwrapper>
          Your records also have the results of medical tests, treatments,
          medicines, and any notes doctors make about you and your health.
          Medical records aren't only about your physical health. They also
        </Textwrapper>
        <ImgAndFeaturesWrapper>
          <ImgWrapper>
            <Image src={HospitalImg} alt="img" className="Img" />
          </ImgWrapper>
          <FeaturesWrapper>
            <Feature>
              <FeaturesHeading>Seamless & Secure med Records</FeaturesHeading>
              <FeaturesDetails>
                Your records also have the results of medical tests, treatments,
                medicines, and any notes doctors make about you and your health.
                Medical records aren't only about your physical health. They
                also
              </FeaturesDetails>
            </Feature>
            <Feature>
              <FeaturesHeading>Seamless & Secure med Records</FeaturesHeading>
              <FeaturesDetails>
                Your records also have the results of medical tests, treatments,
                medicines, and any notes doctors make about you and your health.
                Medical records aren't only about your physical health. They
                also
              </FeaturesDetails>
            </Feature>
            <Feature>
              <FeaturesHeading>Seamless & Secure med Records</FeaturesHeading>
              <FeaturesDetails>
                Your records also have the results of medical tests, treatments,
                medicines, and any notes doctors make about you and your health.
                Medical records aren't only about your physical health. They
                also
              </FeaturesDetails>
            </Feature>
          </FeaturesWrapper>
        </ImgAndFeaturesWrapper>
      </TextAndHeadingwrapper>
    </FeaturesMainwrapper>
  );
};

// styles
const FeaturesMainwrapper = styled.div``;
const TextAndHeadingwrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
const Headingwrapper = styled.div`
  font-size: 60px;
  padding-left: 440px;
`;
const Textwrapper = styled.div`
  width: 100%;
  max-width: 950px;
  font-family: OutfitRegular;
  font-size: 22px;
  line-height: 29px;
  color: rgb(229, 229, 229);
`;
const ImgAndFeaturesWrapper = styled.div`
  display: flex;
  width: 100%;
  max-width: 950px;
  align-items: center;
  justify-content: space-between;
  gap: 50px;
  padding-top: 20px;
`;
const ImgWrapper = styled.div`
  .Img {
    width: 400px;
    height: 300px;
  }
`;
const FeaturesWrapper = styled.div``;
const Feature = styled.div`
  padding: 1vw;
  :hover {
    background-color: blue;
    cursor: pointer;
    border-radius: 2%;
  }
`;
const FeaturesHeading = styled.div`
  font-family: OutfitMedium;
  font-style: normal;
  font-weight: 400;
  font-size: 24px;
  line-height: 24px;
  letter-spacing: -0.24px;
  color: rgb(255, 255, 255);
`;
const FeaturesDetails = styled.div`
  align-items: center;
  font-family: OutfitRegular;
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  line-height: 24px;
  color: rgb(229, 229, 229);
  mix-blend-mode: luminosity;
`;
